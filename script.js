let tabs = document.querySelectorAll(".tabs-title");
let contents = document.querySelectorAll(".content");

for (let i = 0; i < tabs.length; i++) {
  tabs[i].addEventListener("click", (event) => {
    let tabsChildren = event.target.parentElement.children;
    for (let t = 0; t < tabsChildren.length; t++) {
      tabsChildren[t].classList.remove("tabs_active");
    }
    tabs[i].classList.add("tabs_active");
    let ContentChildren =
      event.target.parentElement.nextElementSibling.children;
    for (let c = 0; c < ContentChildren.length; c++) {
      ContentChildren[c].classList.remove("content_active");
    }
    contents[i].classList.add("content_active");
  });
}
